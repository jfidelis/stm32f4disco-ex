import time
from pyb import Pin

class WiegandClass:
    def __init__(self):
        print('init')
        #Reader 1
        W0_D0 = 'PE10' 
        W0_D1 = 'PE11' 

        #Reader 2
        W1_D0 = 'PE12'
        W1_D1 = 'PE13'  

        global pinW0D0
        global pinW0D1
        global pinW1D0
        global pinW1D1        

        pinW0D0 = Pin(W0_D0, Pin.OUT_PP)
        pinW0D1 = Pin(W0_D1, Pin.OUT_PP)
        pinW1D0 = Pin(W1_D0, Pin.OUT_PP)
        pinW1D1 = Pin(W1_D1, Pin.OUT_PP)
        pinW0D0.high()
        pinW0D1.high()
        pinW1D0.high()
        pinW1D1.high()


    def outwiegbit(self, b, index):
        if(index == 0):
            if(b == 0):
                pinW0D0.low()
                time.sleep_us(80)
                pinW0D0.high()
                time.sleep_us(240)
            else:
                pinW0D1.low()
                time.sleep_us(80)
                pinW0D1.high()
                time.sleep_us(240)                
        else:
            if(b == 0):
                pinW1D0.low()
                time.sleep_us(80)
                pinW1D0.high()
                time.sleep_us(240)
            else:
                pinW1D1.low()
                time.sleep_us(80)
                pinW1D1.high()
                time.sleep_us(240)   



    def outwieg26(self, u32, index):
        tmp = u32
        p_even = 0
        p_odd = 0
        # compute parity on trailing group of bits 
        for n in range(12):
            p_odd ^= (tmp & 1)
            tmp >>= 1

        # compute parity on heading group of bits
        for x in range(12, 24):  
            p_even ^= (tmp & 1)
            tmp >>= 1

        # now output data bits framed by parity ones
        self.outwiegbit(p_even, index)
        for n in range(24):
            self.outwiegbit((u32 >> (23-n)) & 1, index)
        
        self.outwiegbit(p_odd, index)

        


